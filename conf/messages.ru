register=Регистрация
register.login=Логин
register.email=Почта
register.password=Пароль
register.repassword=Повтор пароля

login=Войти
login.login=Логин
login.email=Почта
login.password=Пароль

button.doLogin=Войти
button.doRegister=Отправить
button.goToRegister=Регистрация
button.addProductToCatalog=Добавить

addProductToCatalog=Добавление товара
addProductToCatalog.name=Наименование
addProductToCatalog.descr=Описание
addProductToCatalog.cost=Стоимость
addProductToCatalog.image=Изображение

or=или

navbar.main=Главная
navbar.register=Регистрация
navbar.login=Войти
navbar.logout=Выйти
navbar.admin=Админка

addImage=Добавление изображения

addImage.image=изображение

navbar.addImage=Добавить изображение

button.addImage=Отправить

navbar.addProductToCatalog=Добавить товар

validation.format.email=Не верный формат электронной почты

validation.required=Поле должно быть заполнено
validation.required.start=Укажите дату начал лотереи
validation.required.end=Укажите дату окончания лотереи
validation.required.login=Логин должен быть заполнен
validation.required.email=Почта должна быть заполнена
validation.required.password=Пароль должен быть заполнен
validation.required.repassword=Повторите пароль

form.login.validation.invalid_user_or_pass=Не верный логин или пароль
form.register.validation.login_exists=Логин уже занят
form.register.validation.email_exists=Почта уже занята
form.register.validation.pass_equals=Пароли должны совпадать

server.error=Ошибка сервера

form.login.validation.image_id_not_found=Изображение с таким id не найдено.

profile_nav.fill_account=Пополнить баланс
profile_nav.create_lot=Создать лот
profile_nav.account=Баланс: 

admin.users=Пользователи
admin.params=Настройки 

admin.param.id=Id
admin.param.name=Свойство
admin.param.descr=Описание
admin.param.value=Значение
admin.param.actions=Действия


admin.order.id=№
admin.order.login=Заказчик
admin.order.phone=Телефон
admin.order.status=статус
admin.order.actions=Действия

admin.user.id=Id
admin.user.login=Логин
admin.user.email=Почта
admin.user.account=Баланс
admin.user.actions=Действия

admin.images=Изображения
admin.orders=Заказы

admin.image.id=№
admin.image.image=Изображение
admin.image.actions=Действия
admin.image.name=Имя

admin.products=Товары
admin.product.id=Id
admin.product.visible=Видимый
admin.product.name=Наименование
admin.product.cost=Стоимость
admin.product.image_name=Файл изображения
admin.product.image=Изображение
admin.product.actions=Действия

editOrder=Редактирование заказа

product.id=Id
product.name=Наименование
product.cost=Стоимость
product.actions=Действия

catalog.buy=Добавить в корзину
catalog.remove=Удалить из корзины

navbar.products_in_basket=Товаров в корзине: 

basket.id=Id
basket.name=Наименование
basket.count=Количество
basket.one_cost=Стоимость (ед)
basket.sum_cost=Всего
basket.actions=Действия

basket.status=Статус

basket.login=Ваше имя
basket.phone=Введите номер телефона

ebasket.status=Статус

ebasket.login=Имя
ebasket.phone=Телефон 
 

checkout=Отправка заказа

button.doSendBasket=Отправить заказ

addParam=Добавление параметра
addParam.name=Имя параметра
addParam.value=Значение параметра
addParam.descr=Описание параметра
button.addParam=Добавить параметр
navbar.addParam=Добавить параметр

editProduct=Редактирование товара
button.editProductSend=Сохранить

button.saveOrder=Сохранить
catalog.fast_checkout=Мгновенная покупка