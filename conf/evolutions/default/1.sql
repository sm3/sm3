# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table images (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  constraint pk_images primary key (id))
;

create table items (
  id                        bigint auto_increment not null,
  order_id                  bigint,
  product_id                bigint,
  count                     bigint not null,
  constraint pk_items primary key (id))
;

create table orders (
  id                        bigint auto_increment not null,
  owner_id                  bigint,
  status                    varchar(255) not null,
  login                     varchar(255) not null,
  phone                     varchar(255) not null,
  constraint pk_orders primary key (id))
;

create table params (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  value                     varchar(255) not null,
  descr                     varchar(255),
  constraint pk_params primary key (id))
;

create table products (
  id                        bigint auto_increment not null,
  visible                   tinyint(1) default 0 not null,
  cost                      bigint not null,
  name                      varchar(255) not null,
  descr                     varchar(255) not null,
  image_id                  bigint,
  constraint pk_products primary key (id))
;

create table security_role (
  id                        bigint auto_increment not null,
  name                      varchar(255) not null,
  constraint uq_security_role_name unique (name),
  constraint pk_security_role primary key (id))
;

create table users (
  id                        bigint auto_increment not null,
  phone                     varchar(255),
  login                     varchar(255),
  email                     varchar(255) not null,
  password                  varchar(255) not null,
  hash                      varchar(255),
  ip                        varchar(255),
  constraint pk_users primary key (id))
;


create table users_security_role (
  users_id                       bigint not null,
  security_role_id               bigint not null,
  constraint pk_users_security_role primary key (users_id, security_role_id))
;
alter table items add constraint fk_items_order_1 foreign key (order_id) references orders (id) on delete restrict on update restrict;
create index ix_items_order_1 on items (order_id);
alter table items add constraint fk_items_product_2 foreign key (product_id) references products (id) on delete restrict on update restrict;
create index ix_items_product_2 on items (product_id);
alter table orders add constraint fk_orders_owner_3 foreign key (owner_id) references users (id) on delete restrict on update restrict;
create index ix_orders_owner_3 on orders (owner_id);
alter table products add constraint fk_products_image_4 foreign key (image_id) references images (id) on delete restrict on update restrict;
create index ix_products_image_4 on products (image_id);



alter table users_security_role add constraint fk_users_security_role_users_01 foreign key (users_id) references users (id) on delete restrict on update restrict;

alter table users_security_role add constraint fk_users_security_role_security_role_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table images;

drop table items;

drop table orders;

drop table params;

drop table products;

drop table security_role;

drop table users;

drop table users_security_role;

SET FOREIGN_KEY_CHECKS=1;

