package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Image;
import models.Item;
import models.Order;
import models.Param;
import models.Product;
import models.SecurityRole;
import models.User;
import play.Routes;
import play.data.Form;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import util.Helper;
import views.html.addImage;
import views.html.addParam;
import views.html.addProductToCatalog;
import views.html.admin;
import views.html.basket;
import views.html.index;
import views.html.login;
import views.html.register;
import views.html.editOrder;
import views.html.editProduct;
import views.html.sendBasketSuccess;
import views.html.base.basketPage;
import views.html.base.catalogPage;
import views.html.base.imagesPage;
import views.html.base.info;
import views.html.base.ordersPage;
import views.html.base.pagesCount;
import views.html.base.paramsPage;
import views.html.base.productsPage;
import views.html.base.usersPage;
import be.objectify.deadbolt.java.actions.Restrict;
import be.objectify.deadbolt.java.actions.SubjectNotPresent;
import be.objectify.deadbolt.java.actions.SubjectPresent;

import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import com.google.common.net.MediaType;

import controllers.form.AddImageForm;
import controllers.form.AddParamForm;
import controllers.form.AddProductToCatalogForm;
import controllers.form.EditProductForm;
import controllers.form.LoginUserForm;
import controllers.form.RegisterUserForm;
import controllers.form.SendBasketForm;
import controllers.form.EditOrderForm;

public class Application extends Controller {

	public static final String ERROR = "error";
	public static final String WARNING = "warning";
	public static final String SUCCESS = "success";
	
	public static final String PRODUCT_IMAGES_FOLDER = "product_images_folder";
	public static final String PROJECT_NAME = "project_name";
	public static final String FAST_CHECKOUT_BUTTON = "fast checkout button";
	public static final String ADD_REMOVE_TO_FROM_BASKET_BUTTON = "add or remove to from basket button";
	
	private final static Form<LoginUserForm> sLoginUserForm = Form.form(LoginUserForm.class);
	private final static Form<RegisterUserForm> sRegisterUserForm = Form.form(RegisterUserForm.class);
	private final static Form<AddProductToCatalogForm> sAddProductToCatalogForm = Form.form(AddProductToCatalogForm.class);
	private final static Form<SendBasketForm> sSendBasketForm = Form.form(SendBasketForm.class);
	private final static Form<AddImageForm> sAddImageForm = Form.form(AddImageForm.class);
	private final static Form<AddParamForm> sAddParamForm = Form.form(AddParamForm.class);
	private final static Form<EditProductForm> sEditProductForm = Form.form(EditProductForm.class);
	private final static Form<EditOrderForm> sEditOrderForm = Form.form(EditOrderForm.class);

	private final static Map<String, Param> params = new HashMap<String, Param>();
	
	public static boolean isFastCheckoutButton() {
		return Boolean.parseBoolean(getParamValue(FAST_CHECKOUT_BUTTON));
	}
	
	public static boolean isProductAddRemoveToFromBasketButtons() {
		return Boolean.parseBoolean(getParamValue(ADD_REMOVE_TO_FROM_BASKET_BUTTON));
	}
	
	public static Result index() {
		return ok(index.render());
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result sayHello() {
		//RequestBody body = request().body();
		//JsonNode jsonNode = body.asJson();
		
		Product product = new Product();
		return ok(Json.toJson(product));
	}

	public static String getParamValue(String paramName) {
		Param param = params.get(paramName);
		if(param == null) {
			param = Param.findByName(paramName);
			params.put(paramName, param);
		}
		return param.getValue();		
	}
	
//	public static String getPramaName(String value) {
//		
//	}
	
	public static void setParamValue(String paramName, String value) {
		Param param = params.get(paramName);
		if(param == null) {
			param = Param.findByName(paramName);
			if(param == null) {
				param = new Param();
				param.name = paramName;
				param.value = value;
				param.save();
			} else {
				param.value = value;
				param.update();
			}
			params.put(paramName, param);
		} else {
			param.setValue(value);
			param.update();
		}
	}

	
	public static String getImagesPath() {
		return getParamValue(PRODUCT_IMAGES_FOLDER);
	}
	
	public static String getProjectName() {
		return getParamValue(PROJECT_NAME);
	}
	
	public static User getUser() {
		String id = session().get(User.FIELD_ID);
		String hash = session().get(User.FIELD_HASH);

		if (id == null || hash == null)
			return null;

		return User.findByIdAndHash(id, hash);
	}

	public static Result basket() {
		return ok(basket.render(sSendBasketForm));
	}
	
	@SubjectNotPresent
	public static Result register() {
		return ok(register.render(sRegisterUserForm));
	}

	@SubjectNotPresent
	public static Result processRegister() {
		Form<RegisterUserForm> registerUserForm = sRegisterUserForm.bindFromRequest();
		if (registerUserForm.hasErrors()) {
			return badRequest(register.render(registerUserForm));
		}

		User user = new User();
		user.setEmail(registerUserForm.field(User.FIELD_EMAIL).value());
		user.setPassword(Helper.getDoubleMD5(registerUserForm.field(User.FIELD_PASSWORD).value()));
		user.setRoles(new ArrayList<SecurityRole>());
		user.roles.add(SecurityRole.getOrCreateRole(SecurityRole.ROLE_USER));
		user.save();

		return redirect(routes.Application.login());
	}

	@SubjectNotPresent
	public static Result login() {
		return ok(login.render(sLoginUserForm));
	}
	
	public static Result processBasket() {
		Form<SendBasketForm> sendBasketForm = sSendBasketForm.bindFromRequest();
		if (sendBasketForm.hasErrors()) {
			return badRequest(basket.render(sSendBasketForm));
		}

		Order order = parseOrder(sendBasketForm.field(SendBasketForm.FIELD_BASKET).value());
		order.setStatus(Order.WAITTING_FOR_APPROVAL);
		order.login = sendBasketForm.field(Order.FIELD_LOGIN).value();
		order.phone = sendBasketForm.field(Order.FIELD_PHONE).value();
		order.save();
		
		//clean basket cookie's
		
		return ok(sendBasketSuccess.render(order)); 
	}
	
	@SubjectNotPresent
	public static Result processLogin() {
		Form<LoginUserForm> loginUserForm = sLoginUserForm.bindFromRequest();
		if (loginUserForm.hasErrors()) {
			return badRequest(login.render(loginUserForm));
		}

		User user = User.findByEmail(loginUserForm.field(User.FIELD_EMAIL).value());
		user.hash = Helper.getMD5RandomString();
		user.ip = request().remoteAddress();
		user.update();

		session().put(User.FIELD_ID, user.id.toString());
		session().put(User.FIELD_HASH, user.hash);

		return redirect(routes.Application.index());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result addProductToCatalog() {
		return ok(addProductToCatalog.render(sAddProductToCatalogForm));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result addParam() {
		return ok(addParam.render(sAddParamForm));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result addImage() {
		return ok(addImage.render(sAddImageForm));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result processAddParam() {		
		Form<AddParamForm> addParamForm = sAddParamForm.bindFromRequest();
		if (addParamForm.hasErrors()) {
			return badRequest(addParam.render(addParamForm));
		}
  
        Param param = new Param();
        param.setName(addParamForm.field(Param.FIELD_NAME).value());
        param.setValue(addParamForm.field(Param.FIELD_VALUE).value());
        param.save();

		return redirect(routes.Application.admin());
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result processAddImage() {		
		Form<AddImageForm> addImageForm = sAddImageForm.bindFromRequest();
		if (addImageForm.hasErrors()) {
			return badRequest(addImage.render(addImageForm));
		}
        MultipartFormData body = request().body().asMultipartFormData();
        FilePart picture = body.getFile(Product.FIELD_IMAGE);
        if(picture == null) {
        	flash("error", "Missing file");
        	return badRequest(addImage.render(addImageForm));
        }

        String contentType = picture.getContentType();
                
        String extension = null;
        if(contentType.equals(MediaType.JPEG.toString()))
        	extension = "jpg";
        if(contentType.equals(MediaType.PNG.toString()))
        	extension = "png";
        if(contentType.equals(MediaType.BMP.toString()))
        	extension = "bmp";
        if(contentType.equals(MediaType.GIF.toString()))
        	extension = "gif";
        if(extension == null) {
        	flash("error", "Missing content type. Supported content types are: JPEG, PNG, BMP, GIF");
        	return badRequest(addImage.render(addImageForm));
        }

        File file = picture.getFile();
        if(file == null) {
        	flash("error", "Can't get picture file");
        	return badRequest(addImage.render(addImageForm));        	
        }
        
        String localFileName = Image.getLocalPathByExtension(extension);
        try {
			copyFile(file, new File(Image.getLocalPath(localFileName)));
		} catch (IOException e) {
        	flash("error", "Internal server error");
        	return badRequest(addImage.render(addImageForm));
		}
        
        Image image = new Image();
        image.setName(localFileName);        
        image.save();
        Long id = image.getId();
    	flash(SUCCESS, "Было добавлено новое изображение с индексом: " + id.toString());
		return redirect(routes.Application.admin());
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result processAddProductToCatalog() {		
		Form<AddProductToCatalogForm> addProductToCatalogForm = sAddProductToCatalogForm.bindFromRequest();
		if (addProductToCatalogForm.hasErrors()) {
			return badRequest(addProductToCatalog.render(addProductToCatalogForm));
		}
		
        Product product = new Product();
        product.setName(addProductToCatalogForm.field(Product.FIELD_NAME).value());
        product.setDescr(addProductToCatalogForm.field(Product.FIELD_DESCR).value());
        product.setImage(Image.findById(new Long(addProductToCatalogForm.field(Product.FIELD_IMAGE).value().trim())));
        product.setCost(Long.parseLong(addProductToCatalogForm.field(Product.FIELD_COST).value()));        
        product.setVisible(false);
        product.save();

		return redirect(routes.Application.admin());
	}
	
	public static void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}

	@SubjectPresent
	public static Result logout() {
		User user = getUser();
		if (user != null) {
			user.hash = null;
			user.ip = null;
			user.update();
			user = null;
			session().clear();
		}
		return ok(index.render());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result admin() {
		return ok(admin.render());
	}
	

	//====================== JS actions ======================//
	
	//====================== Отображение страниц с pagging view ====================//
	
	public static Result getCatalogPagesCount(Integer pageSize) {
		return ok(pagesCount.render(Product.allPagesVisible(pageSize).getTotalPageCount()));
	}

	public static Result getCatalogPage(Integer page, Integer pageSize) {
		PagingList<Product> pagingList = Product.allPagesVisible(pageSize);
		Page<Product> localPage = pagingList.getPage(page);
		List<Product> list = localPage.getList();
		return ok(catalogPage.render(list));
	}
	
	public static Result getProductsPagesCount(Integer pageSize) {
		return ok(pagesCount.render(Product.allPages(pageSize).getTotalPageCount()));
	}

	public static Result getProductsPage(Integer page, Integer pageSize) {
		PagingList<Product> pagingList = Product.allPages(pageSize);
		Page<Product> localPage = pagingList.getPage(page);
		List<Product> list = localPage.getList();
		return ok(productsPage.render(list));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getParamsPagesCount(Integer pageSize) {
		return ok(pagesCount.render(Param.allPages(pageSize).getTotalPageCount()));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getParamsPage(Integer page, Integer pageSize) {
		PagingList<Param> pagingList = Param.allPages(pageSize);
		Page<Param> localPage = pagingList.getPage(page);
		List<Param> list = localPage.getList();
		return ok(paramsPage.render(list));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getUsersPagesCount(Integer pageSize) {
		return ok(pagesCount.render(User.allPages(pageSize).getTotalPageCount()));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getUsersPage(Integer page, Integer pageSize) {
		PagingList<User> pagingList = User.allPages(pageSize);
		Page<User> localPage = pagingList.getPage(page);
		List<User> list = localPage.getList();
		return ok(usersPage.render(list));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getOrdersPagesCount(Integer pageSize) {
		return ok(pagesCount.render(Order.allPages(pageSize).getTotalPageCount()));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getOrdersPage(Integer page, Integer pageSize) {
		PagingList<Order> pagingList = Order.allPages(pageSize);
		Page<Order> localPage = pagingList.getPage(page);
		List<Order> list = localPage.getList();
		return ok(ordersPage.render(list));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getImagesPagesCount(Integer pageSize) {
		return ok(pagesCount.render(Image.allPages(pageSize).getTotalPageCount()));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result getImagesPage(Integer page, Integer pageSize) {
		PagingList<Image> pagingList = Image.allPages(pageSize);
		Page<Image> localPage = pagingList.getPage(page);
		List<Image> list = localPage.getList();
		return ok(imagesPage.render(list));
	}
	
	
	public static Order parseOrder(String basket) {
		Order order = new Order();
		order.items = new ArrayList<Item>();		
		if(basket != null) {
			String[] items = basket.split("\\|");
			for(String strItem : items) {
				String strItems[] = strItem.split(":");
				// id:count:cost_per_item
				if(strItems.length == 3) {
					try {
						Long id = Long.parseLong(strItems[0].trim());
						Long count = Long.parseLong(strItems[1].trim());
						
						Product product = Product.findById(id);
						if(product != null) {
							Item item = new Item();
							item.product = product;
							item.count = count;
							item.order = order;
							order.items.add(item);
						}
					} catch (NumberFormatException e) {
						
					}
				}
			}
		}
		return order;		
	}

	public static Result getBasketPage(String basket) {
		return ok(basketPage.render(parseOrder(basket)));
	}
	
	//====================== Действия в таблицах ====================//
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result editOrder(Long id) {
		Order order = Order.findById(id);
		if(order == null)
			return badRequest("Can't find order with id = " + id);
		
		Form<EditOrderForm> lEditOrderForm = new Form<EditOrderForm>(EditOrderForm.class);
		EditOrderForm form = new EditOrderForm();
		form.id = order.getId();
		form.login = order.getLogin();
		form.phone = order.getPhone();
		form.status = order.getStatus();
		
		lEditOrderForm = lEditOrderForm.fill(form);
		
		return ok(editOrder.render(lEditOrderForm, order));
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result processEditOrder() {
		Form<EditOrderForm> editOrderForm = sEditOrderForm.bindFromRequest();

		String strId = editOrderForm.field(Order.FIELD_ID).value();
		Long id = null;
		try {
			id = Long.parseLong(strId);
		} catch(NumberFormatException e) {
			flash(ERROR, "Can't parse id: " + strId);
			return badRequest("error");			
		}
		
		Order order = Order.findById(id);
		if(order == null) {
			flash(ERROR, "Can't find product with id: " + strId);
			return badRequest("error");						
		}
		
		if (editOrderForm.hasErrors()) {
			return badRequest(editOrder.render(editOrderForm, order));
		}

//		String strId = editOrderForm.field(Order.FIELD_ID).value();
//		if(strId == null) {
//			flash(ERROR, "Id is null");
//			return badRequest(editOrder.render(editOrderForm, order));
//		}
				
        order.setLogin(editOrderForm.field(Order.FIELD_LOGIN).value());
        order.setPhone(editOrderForm.field(Order.FIELD_PHONE).value());
        order.setStatus(editOrderForm.field(Order.FIELD_STATUS).value());
        
        order.update();

        flash(SUCCESS, "Товар с индексом " + id + " успешно обновлен.");
		return redirect(routes.Application.admin());
	}

	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result editProduct(Long id) {
		Product product = Product.findById(id);
		if(product == null)
			return badRequest("Can't find product with id = " + id);
		
		Form<EditProductForm> lEditProductForm = new Form<EditProductForm>(EditProductForm.class);
		EditProductForm form = new EditProductForm();
		form.id = product.getId();
		form.cost = product.getCost().toString();
		form.descr = product.getDescr();
		form.image = product.getImage() == null ? "" : product.getId().toString();
		form.name = product.getName();
		
		lEditProductForm = lEditProductForm.fill(form);
		
		return ok(editProduct.render(lEditProductForm));
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result processEditProduct() {
		Form<EditProductForm> editProductForm = sEditProductForm.bindFromRequest();
		if (editProductForm.hasErrors()) {
			return badRequest(editProduct.render(editProductForm));
		}

		String strId = editProductForm.field(Product.FIELD_ID).value();
		if(strId == null) {
			flash(ERROR, "Id is null");
			return badRequest(editProduct.render(editProductForm));
		}
		
		Long id = null;
		try {
			id = Long.parseLong(strId);
		} catch(NumberFormatException e) {
			flash(ERROR, "Can't parse id: " + strId);
			return badRequest(editProduct.render(editProductForm));			
		}
		
		Product product = Product.findById(id);
		if(product == null) {
			flash(ERROR, "Can't find product with id: " + strId);
			return badRequest(editProduct.render(editProductForm));						
		}
				
        product.setName(editProductForm.field(Product.FIELD_NAME).value());
        product.setDescr(editProductForm.field(Product.FIELD_DESCR).value());
        
        String strImageId = editProductForm.field(Product.FIELD_IMAGE).value().trim();
        if(!(strImageId == null || strImageId.length() == 0)) {
    		Long imageId = null;
    		try {
    			imageId = Long.parseLong(strImageId);
            	product.setImage(Image.findById(imageId));
    		} catch(NumberFormatException e) {
    			flash(ERROR, "Can't parse id: " + strId);
    			return badRequest(editProduct.render(editProductForm));			
    		} 
        }
        product.setCost(Long.parseLong(editProductForm.field(Product.FIELD_COST).value()));        
        //product.setVisible(false);
        product.update();

        flash(SUCCESS, "Товар с индексом " + id + " успешно обновлен.");
		return redirect(routes.Application.admin());
	}
	
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result removeImage(Integer id) {
		Image image = Image.findById(new Long(id));
		if(image == null)
			return badRequest();

		
		List<Product> products = Product.findByImage(image);
		String productsWithoutImages = null;
		if(products != null) {
			for(Product product : products) {
				product.setImage(null);
				//product.setVisible(false);
				product.update();
				
				if(productsWithoutImages == null)
					productsWithoutImages = "Следующие продукты остались без изображений: " + 
							product.getId() + ":'" + product.getName() + "'. " +
									"Рекоммендуется сделать их невидимыми из каталога пользователя.";
				else 
					productsWithoutImages = ", " + product.getId() + ":'" + product.getName() + "'";
			}
		}
		
		image.delete();
		
		File file = new File(image.getLocalPath());
		file.delete();
		
		if(productsWithoutImages != null) {
			Map<String, String> messages = new HashMap<String, String>();
			messages.put(WARNING, productsWithoutImages);
			return ok(info.render(messages));
		}
		
		return ok();
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result removeParam(Integer id) {
		Param param = Param.findById(id);
		if(param == null)
			return badRequest();

		params.remove(param.getName());
		param.delete();
		
		return ok();
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result removeProduct(Integer id) {
		Product product = Product.findById(new Long(id));
		if(product == null)
			return badRequest();

		// TODO: update all orders !!!
		// flash message: "сначала удалите заказы: номера заказов "
		product.delete();
		
		return ok();
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result removeOrder(Integer id) {
		Order order = Order.findById(new Long(id));
		if(order == null)
			return badRequest();
		order.delete();
		return ok();
	}



	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeProductVisible(Integer id, Boolean newValue) {
		if(id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}
		
		if(newValue == null) {
			flash("error", "Visible flag can't be null");
			return badRequest();
		}
			
		Product product = Product.findById(new Long(id));
		if(product == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}

		product.setVisible(newValue);
		product.update();
		
		return ok();
	}

	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeParamDescr(Integer id, String newValue) {
		if(id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}
		
		if(newValue == null || newValue.length() == 0) {
			flash("error", "Description can't be null");
			return badRequest();
		}
			
		Param param = Param.findById(id);
		if(param == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}
		
		params.remove(param.getName());
		
		param.setDescr(newValue);
		param.save();
		
		return ok();
	}

	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeParamValue(Integer id, String newValue) {
		if(id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}
		if(newValue == null)
			newValue = "";
		
		Param param = Param.findById(id);
		if(param == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}
		
		params.remove(param.getName());
		
		param.setValue(newValue);
		param.save();
		
		return ok();
	}
	
	@Restrict({ SecurityRole.ROLE_ADMIN })
	public static Result changeParamName(Integer id, String newValue) {
		if(id == null) {
			flash("error", "Id can't be null");
			return badRequest();
		}
		
		if(newValue == null || newValue.length() == 0) {
			flash("error", "Name can't be null");
			return badRequest();
		}
			
		Param param = Param.findById(id);
		if(param == null) {
			flash("error", "Can't find object with id = " + id);
			return badRequest();
		}
		
		params.remove(param.getName());
		
		param.setName(newValue);
		param.save();
		
		return ok();
	}

	
	
	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes.javascriptRouter("appJsRoutes",
				
				routes.javascript.Application.removeProduct(),
				routes.javascript.Application.removeOrder(),
				routes.javascript.Application.removeParam(),
				routes.javascript.Application.removeImage(),
				
				routes.javascript.Application.changeParamName(),
				routes.javascript.Application.changeParamValue(),
				routes.javascript.Application.changeParamDescr(),
				routes.javascript.Application.changeProductVisible(),
				
				routes.javascript.Application.getBasketPage(),
				
				routes.javascript.Application.getUsersPage(),
				routes.javascript.Application.getUsersPagesCount(),

				routes.javascript.Application.getParamsPage(),
				routes.javascript.Application.getParamsPagesCount(),
				
				routes.javascript.Application.getCatalogPage(),
				routes.javascript.Application.getCatalogPagesCount(),
				
				routes.javascript.Application.getProductsPage(),
				routes.javascript.Application.getProductsPagesCount(),
				
				routes.javascript.Application.getImagesPage(),
				routes.javascript.Application.getImagesPagesCount(),
				
				routes.javascript.Application.getOrdersPage(),
				routes.javascript.Application.getOrdersPagesCount())
				
			);
	}
}
