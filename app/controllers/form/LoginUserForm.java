package controllers.form;

import models.User;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class LoginUserForm {

	@Required(message = "validation.required.email")
	public String email;

	@Required(message = "validation.required.password")
	public String password;

	public String validate() {
		User user = User.findByEmailAndPassword(email, password);
		if (user == null) {
			return Messages.get("form.login.validation.invalid_email_or_pass");
		}
		return null;
	}

}
