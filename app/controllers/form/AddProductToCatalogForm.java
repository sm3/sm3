package controllers.form;

import models.Image;
import models.Product;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class AddProductToCatalogForm {

	@Required(message = "validation.required.name")
	public String name;
	
	@Required(message = "validation.required.descr")
	public String descr;
	
	@Required(message = "validation.required.cost")
	public String cost;

	@Required(message = "validation.required.image")
	public String image;

	public String validate() {
		try {
			Long.parseLong(cost);
		} catch (NumberFormatException e) {
			return Messages.get("form.login.validation.wrong_cost_format");
		}
		
		Product product = Product.findByName(name);
		if (product != null) {
			return Messages.get("form.login.validation.product_already_exists");
		}
		
		Image image = Image.findById(Long.parseLong(this.image.trim()));
		if(image == null) {
			return Messages.get("form.login.validation.image_id_not_found");
		}
		
		return null;
	}
	
}
