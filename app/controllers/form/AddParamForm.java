package controllers.form;

import models.Param;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class AddParamForm {

	@Required(message = "validation.required.name")
	public String name;

	@Required(message = "validation.required.descr")
	public String descr;
	
	@Required(message = "validation.required.value")
	public String value;

	public String validate() {
		Param param = Param.findByName(name);
		if (param != null) {
			return Messages.get("form.login.validation.param_already_exists");
		}
		
		return null;
	}
	
}
