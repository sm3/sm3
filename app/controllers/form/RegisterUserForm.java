package controllers.form;

import models.User;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class RegisterUserForm {

	@Email(message = "validation.format.email")
	@Required(message = "validation.required.email")
	public String email;

	@Required(message = "validation.required.password")
	public String password;

	public String validate() {
		User user = User.findByEmail(email);
		if (user != null) {
			return Messages.get("form.register.validation.email_exists");
		}

		return null;
	}

}
