package controllers.form;

import models.Product;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class EditProductForm {

	@Required(message = "validation.required.id")
	public Long id;
	
	@Required(message = "validation.required.name")
	public String name;
	
	@Required(message = "validation.required.descr")
	public String descr;
	
	@Required(message = "validation.required.cost")
	public String cost;

	public String image;

	public String validate() {
		Product product = Product.findById(id);
		if (product == null) {
			return Messages.get("form.login.validation.cannot_find_product_with_id");
		}
		
		try {
			Long.parseLong(cost);
		} catch (NumberFormatException e) {
			return Messages.get("form.login.validation.wrong_cost_format");
		}
				
		return null;
	}
	
}
