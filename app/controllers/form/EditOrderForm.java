package controllers.form;

import models.Order;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class EditOrderForm {

	public final static String FIELD_BASKET = "basket";

	@Required(message = "validation.required.id")
	public Long id;
	
	@Required(message = "validation.required.login")
	public String login;

	@Required(message = "validation.required.phone")
	public String phone;
	
	@Required(message = "validation.required.status")
	public String status;


	public String validate() {
		
		Order order = Order.findById(id);
		if (order == null) {
			return Messages.get("form.login.validation.cannot_find_order_with_id");
		}

		
				
		return null;

	}

}
