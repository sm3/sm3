package controllers.form;

import play.data.validation.Constraints.Required;

public class SendBasketForm {

	public final static String FIELD_BASKET = "basket";
	
	@Required(message = "validation.required.basket")
	public String basket;

	@Required(message = "validation.required.login")
	public String login;

	@Required(message = "validation.required.phone")
	public String phone;

	public String validate() {
		return null;
	}

}
