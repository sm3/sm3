package security;

import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import be.objectify.deadbolt.java.DynamicResourceHandler;
import be.objectify.deadbolt.core.models.Subject;

import controllers.Application;

import play.mvc.Http;
import play.mvc.Result;

import views.html.accessFailed;

public class AppDeadboltHandler extends AbstractDeadboltHandler
{

    @Override
    public Result beforeAuthCheck(Http.Context context) {
        // returning null means that everything is OK.  Return a real result if you want a redirect to a login page or
        // somewhere else
        return null;
    }

    @Override
    public Subject getSubject(Http.Context context) {
        //String id = context.session().get(User.FIELD_ID);
        //String hash = context.session().get(User.FIELD_HASH);
        //return User.findByIdAndHash(id, hash);
	return Application.getUser();
    }

    @Override
    public DynamicResourceHandler getDynamicResourceHandler(Http.Context context) {
        //return new MyDynamicResourceHandler();
	return null;
    }
	
    @Override
    public Result onAccessFailure(Http.Context context,
                                String content) {
        return ok(accessFailed.render());
    }

}
