#функция pagging:
#
#вход:
#	el            - контейнер с навигатором и контейнером результатов
#	elresid       - имя контейнера, в который будут заливаться результаты
#   prevId        - идентификатор кнопки prev (по умолчанию класс previous)
#   nextId        - идентификатор кнопки next (по умолчанию класс next)
#	pageSize      - сколько результатов на страницу (по умолчанию 10)
#   curPage       - номер текущей страницы (по умолчанию 0)
#	getPagesCount - имя функции запроса страницы результатов по номеру
#	getPage       - имя функции запроса страницы
#

class PaggingView extends Backbone.View
    events:
        "click .previous"  : "previousPage"
        "click .next"      : "nextPage"
    initialize: =>
        @curPage  = 0
        if @options.curPage?
            @curPage = @options.curPage
        @pageSize = 10
        if @options.pageSize?
            @pageSize = @options.pageSize
        @prevId = ".previous"
        if @options.prevId?
            @prevId = @options.prevId
        @nextId = ".next"
        if @options.nextId?
            @nextId = @options.nextId
        @options.getPagesCount @updateMaxPagesInit, @pageSize
    updateMaxPagesInit: (data) =>
        @updateMaxPages data
        @updateTable()
        @updatePaginator()
        if @maxPages < 2
            if(!$(@el).find(@prevId).hasClass "disabled")
                $(@el).find(@prevId).addClass "disabled"
            if(!$(@el).find(@mextId).hasClass "disabled")
                $(@el).find(@nextId).addClass "disabled"        
    updateMaxPages: (data) =>
        @maxPages = parseInt data
    previousPage: (e) =>
        e.preventDefault()
        if(@curPage > 0)
            @curPage--
            @updateTable()
            @updatePaginator()
    nextPage: (e) =>
        e.preventDefault()
        if(@curPage + 1 < @maxPages)
            @curPage++
            @updateTable()
            @updatePaginator()
    updateTable: =>
        @removeTableData()
        @options.getPage @appendTableData, @pageSize, @curPage
    removeTableData: =>
        $(@el).find(@options.elresid).children().remove()
    appendTableData: (data) =>
        $(@el).find(@options.elresid).append data
    updatePaginator: =>
        if @curPage == 0
            if(!$(@el).find(@prevId).hasClass "disabled")
                $(@el).find(@prevId).addClass "disabled"
        else
            if($(@el).find(@prevId).hasClass "disabled")
               $(@el).find(@prevId).removeClass "disabled"
        if @curPage + 1 == @maxPages 
            if(!$(@el).find(@nextId).hasClass "disabled")
                $(@el).find(@nextId).addClass "disabled"
        else
            if($(@el).find(@nextId).hasClass "disabled")
                $(@el).find(@nextId).removeClass "disabled"
                
#=========== модель товара в корзине ===============
class BasketItem extends Backbone.Model
    defaults:
        count: 1
    inc: =>
    	@set 'count', (parseInt @get('count'))+1
    dec: =>
    	@set 'count', (parseInt @get('count'))-1
    getCost: =>
        (parseInt @get('cost'))*(parseInt @get('count'))
	    
#=========== модель корзины ========================
class Basket extends Backbone.Collection
    model: BasketItem
    initialize: =>
        basketString = $.cookie("basket");
        if basketString?
            for strItem in basketString.split '|'
                splittedItems = strItem.split ':'
                item = new BasketItem
                    id: parseInt splittedItems[0]
                    count: parseInt splittedItems[1] 
                    cost: parseInt splittedItems[2]
                @add item
                #@addItemInCookie item
    getCount: =>
        counter = 0
        counter += parseInt item.get('count') for item in @models
        counter
    getCost: =>
        cost = 0
        cost += parseInt item.getCost() for item in @models
        cost
    addProduct: (id, cost) =>
        item = @get id
        if item?
            item.inc()
            @updateItemInCookie item
        else
            item = new BasketItem
                id: id
                cost: cost
            @add item
            @addItemInCookie item
    updateItemInCookie: (item) =>
        basketString = $.cookie "basket"
        if basketString?
            $.cookie "basket", basketString.replace new RegExp(item.get('id') + ":\\d+:\\d+"), item.get('id') + ":" + item.get('count') + ":" + item.get('cost') 
        else
            @addItemInCookie item
    addItemInCookie: (item) =>
        #if @isExistsItemInCookie item
        #    return
        basketString = $.cookie "basket"
        if basketString?
            $.cookie "basket", basketString + '|' + item.get('id') + ':' + item.get('count') + ':' + item.get('cost')
        else    
            $.cookie "basket", item.get('id') + ':' + item.get('count') + ':' + item.get('cost')
    removeProduct: (id) =>
        item = @get id
        if item?
            item.dec()
            if parseInt(item.get 'count') == 0
                @remove item
                @removeItemInCookie item
            else
                @updateItemInCookie item
    isExistsItemInCookie: (item) =>
        basketString = $.cookie("basket");
        if basketString?
            for strItem in basketString.split '|'
                if parseInt(item.get 'id') != parseInt(strItem.split(':')[0])
                    return true
        return false
    removeItemInCookie: (item) =>
        basketString = $.cookie("basket");
        if basketString?
            newBasketString = ""
            for strItem in basketString.split '|'
                if parseInt(item.get 'id') != parseInt(strItem.split(':')[0])
                    newBasketString += "|" if newBasketString.length != 0
                    newBasketString += strItem
            if(newBasketString.length == 0)
                $.cookie "basket", null
            else
                $.cookie "basket", newBasketString
    clean: ->
        for item in @models
            @removeProduct item.get('id') while parseInt(item.get 'count') > 0
                

#======================= представление панели корзины ======================

class BasketPanelView extends Backbone.View
    initialize: =>
        @basket = @options.basket
        @basket.bind 'add', @productAdded 
        @basket.bind 'change', @productChanged
        @basket.bind 'remove', @productRemoved
        @refreshBasketPanel()
    setCatalogView: (catView) =>
        @catalogView = catView
    productAdded: (item) =>
        @updateBasketPanel item
    productChanged: (item) =>
        @updateBasketPanel item
    productRemoved: (item) =>
        @updateBasketPanel item
    refreshBasketPanel: =>
        $(@el).html @basket.getCount() + ", cost " + @basket.getCost()
    updateBasketPanel: (item) =>
        if @catalogView?
            @catalogView.updateProductView item
        @refreshBasketPanel()

#======================= представление ордера для checkout страницы =========================
class OrderView extends Backbone.View
    initialize: =>
        @basket = @options.basket
        @initTable()
    initTable: =>
        appJsRoutes.controllers.Application.getBasketPage($.cookie "basket").ajax
            context: this
            success: (data) =>
                $(@el).find("tbody:last").children().remove()
                $(@el).find("tbody:last").append data
                @refreshOrderView()
    refreshOrderView: =>
        for item in $(@el).find("tbody").children()
            if $(item).find(".actions_item_in_basket_init").size() > 0
                $(item).find(".actions_item_in_basket_init").removeClass("actions_item_in_basket_init")
	            $(item).find(".btn_product_inc").on 'click', (e) =>
                    e.preventDefault()
                    itemArea = $(e.target).parent().parent().parent()
                    productId = parseInt itemArea.find(".product_id").html()
                    @basket.addProduct productId
                    localItem = @basket.get productId
                    itemArea.find(".item_count").html(localItem.get 'count')
                    itemArea.find(".item_sum_cost").html(localItem.getCost())
                $(item).find(".btn_product_dec").on 'click', (e) =>
                    e.preventDefault()
                    itemArea = $(e.target).parent().parent().parent()
                    productId = parseInt itemArea.find(".product_id").html()
                    @basket.removeProduct productId
                    localItem = @basket.get productId
                    if localItem?
                        itemArea.find(".item_count").html(localItem.get 'count')
                        itemArea.find(".item_sum_cost").html(localItem.getCost())
                    else
                    	itemArea.remove()
                $(item).find(".btn_product_remove").on 'click', (e) =>
                    e.preventDefault()
                    itemArea = $(e.target).parent().parent().parent()
                    productId = parseInt itemArea.find(".product_id").html()
                    localItem = @basket.get productId 
                    @basket.removeProduct productId while parseInt(localItem.get 'count') > 0
                    itemArea.remove()
                

#======================= главный контроллер ==========================#
#
# слушает события каталога, если добавили в корзину товар,
# то меняем модель корзины, в следствие смены модели корзины, 
# меняется представление корзины и т.д.
#
# корзина существует всегда, а вот каталог может не существовать
#
# если существует каталог - мы создаем один контроллер, иначе другой
#
#=====================================================================#



#======================= представление каталога для выбора товара ===========================
class CatalogView extends PaggingView
    constructor: (basket) ->
        super
            el: $("#catalog")
            elresid: "#catalog_container"
            pageSize: 6
            getPagesCount: (f, pageSize) =>
                appJsRoutes.controllers.Application.getCatalogPagesCount(pageSize).ajax
                    success: f
            getPage: (f, pageSize, curPage) =>
                appJsRoutes.controllers.Application.getCatalogPage(curPage, pageSize).ajax
                    success: f
            basket: basket
    initialize: ->
        super
        @basket = @options.basket.basket
    updateProductView: (item) =>
        #alert "update"
    removeTableData: =>
        if $(@el).find(".btn_add_to_basket").length > 0
            $(button).unbind 'click' for button in $(@el).find ".btn_add_to_basket"
        if $(@el).find(".btn_remove_from_basket").length > 0
            $(button).unbind 'click' for button in $(@el).find ".btn_remove_from_basket"
        if $(@el).find(".btn_add_to_basket_and_checkout").length > 0
            $(button).unbind 'click' for button in $(@el).find ".btn_add_to_basket_and_checkout"
        super
    appendTableData: (data) =>
        super
        for button in $(@el).find(".btn_add_to_basket_and_checkout")
            $(button).on 'click', (e) =>
                e.preventDefault()
                id = parseInt($(e.target).parent().parent().parent().find('.productId').html())
                @basket.addProduct id, parseInt($(e.target).parent().parent().parent().find('.productCost').html())
                if (@basket.get id)?
                    $(e.target).parent().parent().find(".btn_remove_from_basket").show()
                window.location.replace("basket");
        for button in $(@el).find(".btn_add_to_basket")
            $(button).on 'click', (e) =>
                e.preventDefault()
                id = parseInt($(e.target).parent().parent().parent().find('.productId').html())
                @basket.addProduct id, parseInt($(e.target).parent().parent().parent().find('.productCost').html())
                if (@basket.get id)?
                    $(e.target).parent().parent().find(".btn_remove_from_basket").show()
        for button in $(@el).find(".btn_remove_from_basket")
            $(button).on 'click', (e) =>
                e.preventDefault()
                id = parseInt($(e.target).parent().parent().parent().find('.productId').html())
                @basket.removeProduct id
                if !(@basket.get id)?
                    $(e.target).hide()
            if !(@basket.get parseInt($(button).parent().parent().parent().find('.productId').html()))?
                $(button).hide()
            



class UsersView extends PaggingView
    constructor: ->
        super
            el: $("#users_tab")
            elresid: "tbody:last"
            pageSize: 6
            getPagesCount: (f, pageSize) =>
                appJsRoutes.controllers.Application.getUsersPagesCount(pageSize).ajax
                    success: f
            getPage: (f, pageSize, curPage) =>
                appJsRoutes.controllers.Application.getUsersPage(curPage, pageSize).ajax
                    success: f
 
        
class ProductsView extends PaggingView
    constructor: ->
        super
            el: $("#products_tab")
            elresid: "tbody:last"
            pageSize: 6
            getPagesCount: (f, pageSize) =>
                appJsRoutes.controllers.Application.getProductsPagesCount(pageSize).ajax
                    success: f
            getPage: (f, pageSize, curPage) =>
                appJsRoutes.controllers.Application.getProductsPage(curPage, pageSize).ajax
                    success: f   

        
class ParamsView extends PaggingView
    constructor: ->
        super
            el: $("#params_tab")
            elresid: "tbody:last"
            pageSize: 6
            getPagesCount: (f, pageSize) =>
                appJsRoutes.controllers.Application.getParamsPagesCount(pageSize).ajax
                    success: f
            getPage: (f, pageSize, curPage) =>
                appJsRoutes.controllers.Application.getParamsPage(curPage, pageSize).ajax
                    success: f  

class OrdersView extends PaggingView
    constructor: ->
        super
            el: $("#orders_tab")
            elresid: "tbody:last"
            pageSize: 6
            getPagesCount: (f, pageSize) =>
                appJsRoutes.controllers.Application.getOrdersPagesCount(pageSize).ajax
                    success: f
            getPage: (f, pageSize, curPage) =>
                appJsRoutes.controllers.Application.getOrdersPage(curPage, pageSize).ajax
                    success: f       
                    
class ImagesView extends PaggingView
    constructor: ->
        super
            el: $("#images_tab")
            elresid: "tbody:last"
            pageSize: 6
            getPagesCount: (f, pageSize) =>
                appJsRoutes.controllers.Application.getImagesPagesCount(pageSize).ajax
                    success: f
            getPage: (f, pageSize, curPage) =>
                appJsRoutes.controllers.Application.getImagesPage(curPage, pageSize).ajax
                    success: f                         
                    
#================================ представление формы checkout =====================#
class CheckoutFormView extends Backbone.View
    initialize: ->
        $(@el).find("#basket_field").hide()
        @basket = @options.basket
        @basket.bind 'add', @refreshForm 
        @basket.bind 'change', @refreshForm
        @basket.bind 'remove', @refreshForm
        @refreshForm()
    refreshForm: ->
        $(@el).find("#basket_field").find("#basket").val($.cookie "basket")
    
                    
#================================ Главное представление =====================

class AppView extends Backbone.View
    initialize: ->
        # модель корзины существует всегда
        @basket = new Basket
        
        # представление корзины, если оно есть
        if $(@el).find("#products_counter").length > 0
            basketPanelView = new BasketPanelView
                el:  $(@el).find("#products_counter")
                basket: @basket
        
        # если на странице есть каталог, то создаем его представление
        if $(@el).find("#catalog").length > 0
            catalogView = new CatalogView
                basket: @basket
            if basketPanelView?
                basketPanelView.setCatalogView catalogView
        
        # если на странице есть ордер, то создаем его представление        
        if $(@el).find("#basket_checkout").length > 0
            orderView = new OrderView 
                el: $(@el).find("#basket_checkout") 
                basket: @basket
        
        # если есть на странице checkout форма, то создаем ее представление        
        if $(@el).find("#sendBasket").length > 0
            checkoutFormView = new CheckoutFormView
                el: $(@el).find("#sendBasket")
                basket: @basket
        
        # если на странице есть вьюха пользователей, то создаем ее представление
        if $(@el).find("#users_tab").length > 0
            usersView = new UsersView

        # если на странице есть вьюха продуктов, то создаем ее представление        
        if $(@el).find("#products_tab").length > 0            
	        productsView = new ProductsView
        
        # если на странице есть вьюха параметров, то создаем ее представление
        if $(@el).find("#params_tab").length > 0        
            paramsView = new ParamsView
            
        # если на странице есть вьюха ордеров, то создаем ее представление
        if $(@el).find("#orders_tab").length > 0        
            ordersView = new OrdersView    
            
        # вьюха картинок
        if $(@el).find("#images_tab").length > 0        
            imagesView = new ImagesView         
                
        # очищаем корзину, если мы на странице успешной отправки заказа
        if $(@el).find("#successSendBasket").length > 0
            @basket.clean()

#============================== Главный роутер ===============================

class AppRouter extends Backbone.Router
    initialize: ->
        @currentApp = new AppView
            el: $(document)
	                             

#================================ INIT APP ===========================
$ ->
    appRouter = new AppRouter
    $.fn.editable.defaults.mode = 'inline'           

                    