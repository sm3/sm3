import java.util.ArrayList;

import models.Param;
import models.SecurityRole;
import models.User;
import play.Application;
import play.GlobalSettings;

import com.avaje.ebean.Ebean;

public class Global extends GlobalSettings {
	@Override
	public void onStart(Application application) {

		if (User.find.findRowCount() == 0) {
			User user = new User();
			user.login = "iceberg";
			user.password = "b1520bfe7e0b2d4415900242c1f932df";
			user.email = "cromlehg@gmail.com";
			user.roles = new ArrayList<SecurityRole>();
			user.roles.add(SecurityRole
					.getOrCreateRole(models.SecurityRole.ROLE_ADMIN));

			user.save();
			Ebean.saveManyToManyAssociations(user, "roles");
		}

		if (Param.find.findRowCount() == 0) {
			Param param = new Param();
			param.name = controllers.Application.PRODUCT_IMAGES_FOLDER;
			param.descr = "Директория для расположения изображений.";
			param.value = "images/products/";
			param.save();
			
			param = new Param();
			param.name = controllers.Application.PROJECT_NAME;
			param.descr = "Имя проекта, отображается на главной странице вверху";
			param.value = "Project name";
			param.save();
			
			param = new Param();
			param.name = controllers.Application.FAST_CHECKOUT_BUTTON;
			param.descr = "Если включено, то под каждым товаром в каталоге появится кнопка быстрой покупки. При нажатии" +
					"этой кнопки пользователь сразу перейдет на страницу корзины.";
			param.value = "false";
			param.save();
			
			param = new Param();
			param.name = controllers.Application.ADD_REMOVE_TO_FROM_BASKET_BUTTON;
			param.descr = "Если включено, то под товаром в каталоге отображаеются стандартные кнопки - добаить/удалить.";
			param.value = "true";
			param.save();

		}

	}

}
