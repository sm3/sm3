package util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Helper {

	public static Random random = new Random();

	public static String siteName = "SM";

	public static String footerString = "2013 SM";

	public static String getSiteName() {
		return siteName;
	}

	public static String getFooterString() {
		return footerString;
	}

	public static String getDoubleMD5(String input) {
		return getMD5Hash(getMD5Hash(input.trim()));
	}

	public static String getMD5RandomString() {
		return getMD5Hash(generateRandomString());
	}

	public static String getMD5Hash(String input) {
		String hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			hash = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}

	public static String generateRandomString() {
		byte[] temp = new byte[30];
		random.nextBytes(temp);
		return new String(temp);
	}

}
