package models;

public class Types {
	
	// input types
	public static final String IT_BUTTON = "button";
	public static final String IT_CHECKBOX = "checkbox";
	public static final String IT_FILE = "file";
	public static final String IT_IMAGE = "image";
	public static final String IT_HIDDEN = "hidden";
	public static final String IT_PASSWORD = "password";
	public static final String IT_RADIO = "radio";
	public static final String IT_RESET = "reset";
	public static final String IT_SUBMIT = "submit";
	public static final String IT_TEXT = "text";
	public static final String IT_COLOR = "color";
	public static final String IT_DATE = "date";
	public static final String IT_DATETIME = "datetime";
	public static final String IT_DATETIME_LOCAL = "datetime-local";
	public static final String IT_EMAIL = "email";
	public static final String IT_MONTH = "month";
	public static final String IT_NUMBER = "number";
	public static final String IT_RANGE = "range";
	public static final String IT_SEARCH = "search";
	public static final String IT_TEL = "tel";
	public static final String IT_TIME = "time";
	public static final String IT_URL = "url";
	public static final String IT_WEEK = "week";
	
	// param types
	public static final String PT_BOOLEAN = "boolean"; // - checkbox
	public static final String PT_STRING = "string"; // - single line string - text


}
