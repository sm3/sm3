package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;

import com.avaje.ebean.PagingList;

@Entity
@Table(name="products")
public class Product  extends Model  {
	
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_IMAGE = "image";
    public static final String FIELD_COST = "cost";
    public static final String FIELD_CTYPE = "ctype";
    public static final String FIELD_DESCR = "descr";
    public static final String FIELD_VISIBLE = "visible";

    @Id
    public Long id;

    @NotNull
    @Column(nullable=false) 
    public Boolean visible;
    
    @NotNull
    @Column(nullable=false) 
    public Long cost;
    
    @NotNull
    @Column(nullable=false) 
    public String name;
    
    
//    @URL 
//    @Column(name="url", length=1000) 
//    @Length(max=1000) 
//    or @Lob
    
    @NotNull
    @Column(nullable=false) 
    public String descr;
    
    @OneToOne
    //@NotNull
    //@Column(nullable=false) 
    public Image image;
    
    public static final Finder<Long, Product> find = new Finder<Long, Product>(Long.class, Product.class);

	public Long getId() {
		return id;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	public String getDescr() {
		return descr;
	}
	
	public Boolean getVisible() {
		return visible;
	}
	
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image= image;
	}

	public static Product findByName(String name) {
		return find.where().eq(FIELD_NAME, name).findUnique(); 
	}
    
	public static PagingList<Product> allPages(int pageSize) {
		return find.findPagingList(pageSize);
	}
	
	public static PagingList<Product> allPagesVisible(int pageSize) {
		return find.where().eq(FIELD_VISIBLE, true).findPagingList(pageSize);
	}

	public static Product findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}

	public static List<Product> findByImage(Image image) {
		return find.where().eq("image_id", image.getId()).findList();
	}

	
}
