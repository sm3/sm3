package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;
import be.objectify.deadbolt.core.models.Role;

@Entity
public class SecurityRole extends Model implements Role {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String ROLE_ADMIN = "admin";

	public static final String ROLE_USER = "user";

	@Id
	public Long id;

	@NotNull
	@Column(unique = true, nullable = false)
	public String name;

	public static final Finder<Long, SecurityRole> find = new Finder<Long, SecurityRole>(
			Long.class, SecurityRole.class);

	public String getName() {
		return name;
	}

	public static SecurityRole findByName(String name) {
		return find.where().eq("name", name).findUnique();
	}

	public static SecurityRole createRole(String name) {
		SecurityRole role = new SecurityRole();
		role.name = name;
		role.save();
		return role;
	}

	public static SecurityRole getOrCreateRole(String name) {
		SecurityRole role = findByName(name);
		if (role == null) {
			role = createRole(name);
		}
		return role;
	}
}
