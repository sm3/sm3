package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.avaje.ebean.PagingList;

import play.db.ebean.Model;

@Entity
@Table(name = "params")
public class Param extends Model {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_VALUE = "value";
		
	@Id
	public Long id;

	@NotNull
	@Column(nullable = false)
	public String name;

	@NotNull
	@Column(nullable = false)
	public String value;
	
	public String descr;
	
	public static final Finder<Long, Param> find = new Finder<Long, Param>(
			Long.class, Param.class);

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static Param findById(Integer id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public static Param findByName(String name) {
		return find.where().eq(FIELD_NAME, name).findUnique();
	}

	public static PagingList<Param> allPages(int pageSize) {
		return find.findPagingList(pageSize);
	}
	
}
