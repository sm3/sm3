package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.avaje.ebean.PagingList;

import play.db.ebean.Model;

@Entity
@Table(name="orders")
public class Order extends Model {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
    public static final String FIELD_ITEMS = "items";
    public static final String FIELD_STATUS = "status";
    public static final String FIELD_LOGIN = "login";
    public static final String FIELD_PHONE = "phone";

    public static final String STATUS_CLOSED = "closed";
    public static final String STATUS_OPENED = "opened";
    
	public static final String WAITTING_FOR_APPROVAL = "WAITTING_FOR_APPROVAL";
    
    @Id
    public Long id;	
    
    @OneToMany(mappedBy = "order")
    public List<Item> items;

    @OneToOne
    public User owner;
    
    @NotNull
    @Column(nullable=false) 
    public String status;
    
    @NotNull
    @Column(nullable=false) 
    public String login;
    
    @NotNull
    @Column(nullable=false) 
    public String phone;

    public static final Finder<Long, Order> find = new Finder<Long, Order>(Long.class, Order.class);

	
    
	public Long getId() {
		return id;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public static PagingList<Order> allPages(int pageSize) {
		return find.findPagingList(pageSize);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public static Order findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
    
}
