package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;

@Entity
@Table(name="items")
public class Item extends Model {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String FIELD_ID = "id";
    public static final String FIELD_ORDER = "order";
    public static final String FIELD_PRODUCT = "product";
    public static final String FIELD_COUNT = "count";
	
    @Id
    public Long id;
    
    @ManyToOne
    @NotNull
    @Column(nullable=false) 
    public Order order;
    
    @OneToOne
    @NotNull
    @Column(nullable=false) 
    public Product product;
    
    @NotNull
    @Column(nullable=false) 
    public Long count;

    public static final Finder<Long, Item> find = new Finder<Long, Item>(Long.class, Item.class);
    
	public Long getId() {
		return id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

    
    
}
