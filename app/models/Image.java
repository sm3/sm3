package models;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import play.db.ebean.Model;

import com.avaje.ebean.PagingList;

import controllers.Application;

@Entity
@Table(name="images")
public class Image  extends Model  {
	
	public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    @Id
    public Long id;
    
    @NotNull
    @Column(nullable=false) 
    public String name;

    public static final Finder<Long, Image> find = new Finder<Long, Image>(Long.class, Image.class);

	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public static PagingList<Image> allPages(int pageSize) {
		return find.findPagingList(pageSize);
	}

	public static Image findById(Long id) {
		return find.where().eq(FIELD_ID, id).findUnique();
	}
	
	public static Image findByName(String name) {
		return find.where().eq(FIELD_NAME, name).findUnique();
	}

	public String getAssetPath() {
		return Application.getImagesPath() + getName(); 
	}
	
	public String getLocalPath() {
		return getLocalPath(getName()); 
	}

	public static String getLocalPathByExtension(String extension) {
		Integer index = 1;
		String localName = index + "." + extension;
		String localPath = getLocalPath(localName);
		File file = new File(localPath);
		while(file.exists()) {
			index++;
			localName = index + "." + extension;
			localPath = getLocalPath(localName);
			file = new File(localPath);
		}
		return localName; 
	}
	
	public static String getLocalPath(String fName) {
		return "public/" + Application.getImagesPath() + fName; 
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
